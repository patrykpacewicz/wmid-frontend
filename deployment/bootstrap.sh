#!/bin/bash

###############
# check if root
if [ $EUID -ne 0 ]; then
   echo "This script must be run as root" 1>&2
   exit 1
fi

################
# install puppet
UBUNTU_CODENAME=$(lsb_release -c | awk '{print $2}')

PUPPET_URL="http://apt.puppetlabs.com/puppetlabs-release-${UBUNTU_CODENAME}.deb"
PUPPET_OUT="/tmp/puppetlabs-release-${UBUNTU_CODENAME}.deb"
PUPPET_BIN='/usr/bin/puppet'
PUPPET_APPLY_LOG='/var/log/puppet-bootstrap.log'

if [ ! -e $PUPPET_BIN ]; then
    wget -q $PUPPET_URL -O $PUPPET_OUT
    dpkg -i $PUPPET_OUT
    apt-get -qqy update
    apt-get -qqy install puppet
fi

#############
# install app
export FACTER_appDir=${appDir:='/app/frontend'}
export FACTER_appDebug=${appDebug:='0'}
export FACTER_appName=${appName:='frontend'}
export FACTER_appScriptUser=${appScriptUser:=$USER}

puppet module list |grep puppetlabs-stdlib || puppet module install puppetlabs/stdlib
puppet module list |grep puppetlabs-apt    || puppet module install puppetlabs/apt
puppet module list |grep puppetlabs-nodejs || puppet module install puppetlabs/nodejs

$PUPPET_BIN apply --verbose $appDir/deployment/manifest.pp |tee $PUPPET_LOG

