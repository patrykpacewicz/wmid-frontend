class { 'nodejs':
  version     => installed,
  manage_repo => true
}

package { 'grunt-cli':
  ensure   => installed,
  provider => 'npm',
  require  => Class['nodejs']
}
