include apt

apt::ppa { 'ppa:ondrej/php5': }

package { ['php5-cli', 'php5-fpm', 'php5-curl']:
  ensure  => installed,
  require => Apt::Ppa['ppa:ondrej/php5'],
  notify  => Service['php5-fpm']
}

file { "/etc/php5/fpm/pool.d/${cfg_codeName}.conf":
  ensure  => file,
  content => template("${cfg_templatesDir}/php-fpm.cfg"),
  require => Package['php5-fpm'],
  notify  => Service['php5-fpm'],
}

exec { 'install composer.phar':
  command => 'curl -sS https://getcomposer.org/installer | php',
  path    => ['/usr/bin'],
  cwd     => '/usr/bin',
  onlyif  => 'test ! -e /usr/bin/composer.phar',
  require => Package['php5-cli']
}

augeas { "date.timezone" :
  context => "/files",
  changes => [
    "set etc/php5/fpm/php.ini/Date/date.timezone ${cfg_php_timezone}",
    "set etc/php5/cli/php.ini/Date/date.timezone ${cfg_php_timezone}"
  ],
  require => [Package['php5-fpm'], Package['php5-cli']],
  notify  => Service['php5-fpm'],
}

service { 'php5-fpm':
  ensure  => running,
  require => Package['php5-fpm'],
}

if $cfg_debug {
  package { 'php5-xdebug':
    ensure  => installed,
    require => Package['php5-cli']
  }
}
