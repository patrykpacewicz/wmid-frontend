import  'config.pp'

file { $cfg_logDir :
  ensure => directory,
  group  => $cfg_group,
  owner  => $cfg_user
}

package { $cfg_additionalPackages : ensure => installed }

import 'manifests/*.pp'
