module.exports = function(grunt) {
    grunt.initConfig({
        clean:  ["web/fonts", "web/scripts", "web/styles"],
        symlink: {
            development: { src: 'src/PatrykPacewicz/Wmid/Resources/scripts', dest: 'web/scripts'}
        },
        requirejs: {
            options: {
                baseUrl: '.',
                appDir: 'src/PatrykPacewicz/Wmid/Resources/scripts',

                dir: 'web/scripts',
                paths: {
                    'require':     'vendor/require-2.1.8',
                    'jquery':      'vendor/jquery-1.10.2',
                    'bootstrap':   'vendor/bootstrap-3.0.0',
                    'underscore':  'vendor/underscore-1.5.2',
                    'crossfilter': 'vendor/crossfilter-1.3.7',
                    'd3':          'vendor/d3-3.3.13',
                    'dc':          'vendor/dc-2.0.0',
                    'backbone':    'vendor/backbone-1.1.0'
                },

                shim: {
                    bootstrap:   { deps: ["jquery"] },
                    backbone:    { deps: ['underscore', 'jquery'], exports: 'Backbone' },
                    dc:          { deps: ['d3'], exports: 'dc' },
                    crossfilter: { exports: 'crossfilter' },
                    underscore:  { exports: '_' },
                    jquery:      { exports: '$' }
                },

                modules: [ { name: 'app' } ]
            },
            production: {},
            development: { options: { optimize: "none" } }
        },
        less: {
            production: {
                options: { compress: true },
                files:   { 'web/styles/style.css': 'src/**/Resources/less/index.less' }
            },
            development: {
                options: { compress: false },
                files:   { 'web/styles/style.css': 'src/**/Resources/less/index.less' }
            }
        },
        watch: {
            less: {
                files: ['src/**/Resources/less/**/*.less'],
                tasks: ['less:development']
            }
        },
        copy: {
            main: {
                files: [
                    {expand: true, filter: 'isFile', flatten: true, src: ['src/**/Resources/fonts/*'], dest: 'web/fonts/' },
                ]
            }
        }
    });

    grunt.loadNpmTasks('grunt-contrib-clean');
    grunt.loadNpmTasks('grunt-contrib-symlink');
    grunt.loadNpmTasks('grunt-contrib-requirejs');
    grunt.loadNpmTasks('grunt-contrib-less');
    grunt.loadNpmTasks('grunt-contrib-watch');
    grunt.loadNpmTasks('grunt-contrib-copy');

    grunt.registerTask('default', ['build:production']);
    grunt.registerTask('build:production',  ['clean', 'copy', 'less:production',  'requirejs:production']);
    grunt.registerTask('build:development', ['clean', 'copy', 'less:development', 'symlink:development']);
};
