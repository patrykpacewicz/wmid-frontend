<?php

use Doctrine\Common\Annotations\AnnotationRegistry;
use Monolog\Logger;
use PatrykPacewicz\Wmid\Provider\ControllerProvider;
use PatrykPacewicz\Wmid\Provider\ServiceProvider;
use Silex\Application;
use Silex\Provider\MonologServiceProvider;
use Silex\Provider\ServiceControllerServiceProvider;
use Silex\Provider\TwigServiceProvider;
use Silex\Provider\UrlGeneratorServiceProvider;

AnnotationRegistry::registerAutoloadNamespace('JMS\Serializer\Annotation', '../vendor/jms/serializer/src');

$app = new Application();
$app['debug'] = $_SERVER['debug'];

$app->register(new ServiceControllerServiceProvider());
$app->register(new UrlGeneratorServiceProvider());
$app->register(new TwigServiceProvider(), array('twig.path' => __DIR__.'/PatrykPacewicz/Wmid/Resources/views'));
$app->register(
    new ServiceProvider(),
    array('config.file' => __DIR__.'/PatrykPacewicz/Wmid/Resources/config/config.yml')
);
$app->register(
    new MonologServiceProvider(),
    array(
        'monolog.logfile' => sprintf('%s/%s.php.log', $_SERVER['logdir'], $_SERVER['appname']),
        'monolog.level'   => $_SERVER['debug']? Logger::DEBUG : Logger::WARNING,
        'monolog.name'    => $_SERVER['appname']
    )
);

$app->mount('/', new ControllerProvider());

return $app;
