<?php

namespace PatrykPacewicz\Wmid\Provider;

use Guzzle\Http\Client;
use JMS\Serializer\SerializerBuilder;
use PatrykPacewicz\Wmid\Api\ApiClient;
use PatrykPacewicz\Wmid\Api\CommitmentScheme\CommitmentSchemeClient;
use PatrykPacewicz\Wmid\Api\ElectronicVoting\ElectronicVotingClient;
use PatrykPacewicz\Wmid\Api\SecretSharing\SecretSharingClient;
use Silex\Application;
use Silex\ServiceProviderInterface;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Yaml\Yaml;
use Twig_Environment;

class ServiceProvider implements ServiceProviderInterface
{
    public function register(Application $app)
    {
        $this->errorProvider($app);

        $app['pp.serializer'] = $app->share(function ($app) {
            return SerializerBuilder::create()->setDebug($app['debug'])->build();
        });

        $app['pp.http.client'] = $app->share(function ($app) {
            return new Client(
                $app['pp.config']['api']['address'],
                $app['pp.config']['client']['options']
            );
        });

        $app['pp.api.client'] = $app->share(function ($app) {
            return new ApiClient($app['pp.http.client'], $app['pp.serializer']);
        });

        $app['pp.api.secretsharing.client'] = $app->share(function ($app) {
            return new SecretSharingClient(
                $app['pp.api.client'],
                $app['pp.config']['api']['secretsharing']['encode'],
                $app['pp.config']['api']['secretsharing']['decode']
            );
        });

        $app['pp.api.electronicvoting.client'] = $app->share(function ($app) {
            return new ElectronicVotingClient(
                $app['pp.api.client']
            );
        });

        $app['pp.api.commitmentscheme.client'] = $app->share(function ($app) {
            return new CommitmentSchemeClient(
                $app['pp.api.client'],
                $app['pp.config']['api']['commitmentscheme']['all.messages'],
                $app['pp.config']['api']['commitmentscheme']['send'],
                $app['pp.config']['api']['commitmentscheme']['validate'],
                $app['pp.config']['api']['commitmentscheme']['item']
            );
        });
    }

    public function boot(Application $app)
    {
        $app['pp.config'] = Yaml::parse($app['config.file']);
    }

    public function errorProvider(Application $app)
    {
        $app->error(function ($code) use ($app) {
            if ($app['debug']) {
                return null;
            }

            if ($code === 404) {
                return $this->errorAction($app['twig'], 404, 'Page not found');
            }

            return $this->errorAction($app['twig'], 500, 'Internal server error');
        });
    }

    private function errorAction(Twig_Environment $twig, $code, $message)
    {
        $parameters = ['message' => $message, 'code' => $code];
        $errorContent = $twig->render('error.html.twig', $parameters);

        return new Response($errorContent, $code);
    }
}
