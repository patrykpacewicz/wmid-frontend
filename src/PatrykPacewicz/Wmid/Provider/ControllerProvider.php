<?php

namespace PatrykPacewicz\Wmid\Provider;

use PatrykPacewicz\Wmid\Controller\CommitmentSchemeController;
use PatrykPacewicz\Wmid\Controller\DefaultController;
use PatrykPacewicz\Wmid\Controller\ElectronicVotingController;
use PatrykPacewicz\Wmid\Controller\SecretSharingController;
use Silex\Application;
use Silex\ControllerCollection;
use Silex\ControllerProviderInterface;

class ControllerProvider implements ControllerProviderInterface
{
    public function connect(Application $app)
    {
        $this->registerControllers($app);

        $controllers = $app['controllers_factory'];
        $this->configureRouting($controllers);

        return $controllers;
    }

    private function configureRouting(ControllerCollection $controllers)
    {
        $controllers
            ->get('/', "pp.controller.default:defaultAction")
            ->bind('homepage');
        $controllers
            ->get('/imageprocessing', "pp.controller.default:imageProcessingAction")
            ->bind('imageprocessing');

        $controllers
            ->get('/secretsharing', "pp.controller.secretsharing:viewAction")
            ->bind('secretsharing');
        $controllers
            ->post('/secretsharing/encode', "pp.controller.secretsharing:encodeAction")
            ->bind('secretsharing:encode');
        $controllers
            ->post('/secretsharing/decode', "pp.controller.secretsharing:decodeAction")
            ->bind('secretsharing:decode');

        $controllers
            ->get('/commitmentscheme', "pp.controller.commitmentscheme:viewAction")
            ->bind('commitmentscheme');
        $controllers
            ->post('/commitmentscheme/send', "pp.controller.commitmentscheme:sendAction")
            ->bind('commitmentscheme:send');
        $controllers
            ->get('/commitmentscheme/validate/{documentId}', "pp.controller.commitmentscheme:getValidateAction")
            ->bind('commitmentscheme:getValidate');
        $controllers
            ->post('/commitmentscheme/validate/{documentId}', "pp.controller.commitmentscheme:validateAction")
            ->bind('commitmentscheme:validate');

        $controllers
            ->get('/voting', "pp.controller.electronicvoting:viewAction")
            ->bind('voting');
        $controllers
            ->post('/voting', "pp.controller.electronicvoting:voteAction")
            ->bind('voting:vote');

    }

    private function registerControllers(Application $app)
    {
        $app['pp.controller.default'] = $app->share(function ($app) {
            return new DefaultController($app['twig']);
        });
        $app['pp.controller.secretsharing'] = $app->share(function ($app) {
            return new SecretSharingController(
                $app['twig'],
                $app['pp.api.secretsharing.client']
            );
        });

        $app['pp.controller.commitmentscheme'] = $app->share(function ($app) {
            return new CommitmentSchemeController(
                $app['twig'],
                $app['pp.api.commitmentscheme.client'],
                $app['url_generator']
            );
        });

        $app['pp.controller.electronicvoting'] = $app->share(function ($app) {
            return new ElectronicVotingController(
                $app['twig'],
                $app['pp.api.electronicvoting.client'],
                $app['url_generator']
            );
        });

    }
}
