<?php

namespace PatrykPacewicz\Wmid\Api;

use Guzzle\Http\Client;
use JMS\Serializer\Serializer;

class ApiClient
{
    /** @var Client */
    private $client;

    /** @var Serializer */
    private $serializer;

    public function __construct(Client $client, Serializer $serializer)
    {
        $this->client = $client;
        $this->serializer = $serializer;
    }

    public function get($uri, $body = null, $responseClass = null)
    {
        return $this->sendRequest('GET', $uri, $body, $responseClass);
    }

    public function post($uri, $body = null, $responseClass = null)
    {
        return $this->sendRequest('POST', $uri, $body, $responseClass);
    }

    private function sendRequest($method, $uri, $body = null, $responseClass = null)
    {
        if ($body) {
            $body = $this->serializeJson($body);
        }

        $response = $this->client->createRequest($method, $uri, null, $body)->send();

        if ($responseClass) {
            return $this->deserializeJson($response->getBody(true), $responseClass);
        }

        return $response;
    }

    private function serializeJson($data)
    {
        return $this->serializer->serialize($data, 'json');
    }

    private function deserializeJson($data, $type)
    {
        return $this->serializer->deserialize($data, $type, 'json');
    }
}
