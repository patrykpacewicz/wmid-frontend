<?php

namespace PatrykPacewicz\Wmid\Api\CommitmentScheme\Message;

use JMS\Serializer\Annotation\Type;
use JMS\Serializer\Annotation\SerializedName;

class StartMessage
{
    /**
     * @Type("string")
     * @SerializedName("clientHost")
     */
    private $clientHost;

    /** @Type("string") */
    protected $message;

    /** @Type("string") */
    protected $key;

    public function __construct($clientHost, $key, $message)
    {
        $this->clientHost = $clientHost;
        $this->key = $key;
        $this->message = $message;
    }

    /** @return string */
    public function getClientHost()
    {
        return $this->clientHost;
    }

    /** @return string */
    public function getKey()
    {
        return $this->key;
    }

    /** @return string */
    public function getMessage()
    {
        return $this->message;
    }

    /** @param mixed $key */
    public function setKey($key)
    {
        $this->key = $key;
    }

    /** @param mixed $message */
    public function setMessage($message)
    {
        $this->message = $message;
    }
}
