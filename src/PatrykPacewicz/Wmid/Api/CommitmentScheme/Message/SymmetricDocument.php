<?php

namespace PatrykPacewicz\Wmid\Api\CommitmentScheme\Message;

use JMS\Serializer\Annotation\Type;
use JMS\Serializer\Annotation\SerializedName;

class SymmetricDocument extends StartMessage
{
    /**
     * @Type("string")
     * @SerializedName("id")
     */
    private $documentId;

    /**
     * @Type("string")
     * @SerializedName("encodedMessage")
     */
    private $encodedMessage;

    public function __construct($documentId, $encodedMessage, $key, $message)
    {
        $this->documentId = $documentId;
        $this->encodedMessage = $encodedMessage;
        $this->key = $key;
        $this->message = $message;
    }

    /** @return string */
    public function getDocumentId()
    {
        return $this->documentId;
    }

    /** @return string */
    public function getEncodedMessage()
    {
        return $this->encodedMessage;
    }
}
