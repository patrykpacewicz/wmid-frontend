<?php

namespace PatrykPacewicz\Wmid\Api\CommitmentScheme;

use PatrykPacewicz\Wmid\Api\ApiClient;
use PatrykPacewicz\Wmid\Api\CommitmentScheme\Message\StartMessage;
use PatrykPacewicz\Wmid\Api\CommitmentScheme\Message\SymmetricDocument;

class CommitmentSchemeClient
{
    /** @var ApiClient */
    private $client;

    /** @var string */
    private $validateEndpoint;

    /** @var string */
    private $sendEndpoint;

    /** @var string */
    private $getAllMessagesEndpoint;

    /** @var string */
    private $itemEndpoint;

    public function __construct(
        ApiClient $client,
        $getAllMessagesEndpoint,
        $sendEndpoint,
        $validateEndpoint,
        $itemEndpoint
    ) {
        $this->client                 = $client;
        $this->getAllMessagesEndpoint = $getAllMessagesEndpoint;
        $this->sendEndpoint           = $sendEndpoint;
        $this->validateEndpoint       = $validateEndpoint;
        $this->itemEndpoint           = $itemEndpoint;
    }

    public function getAllMessages()
    {
        $responseClass = sprintf("array<%s>", SymmetricDocument::class);
        return $this->client->get($this->getAllMessagesEndpoint, null, $responseClass);
    }

    public function validate(SymmetricDocument $symmetricDocument)
    {
        return $this->client->post($this->validateEndpoint, $symmetricDocument, "boolean");
    }

    public function send(StartMessage $startMessage)
    {
        return $this->client->post($this->sendEndpoint, $startMessage, SymmetricDocument::class);
    }

    public function getItem($id)
    {
        $uri = sprintf($this->itemEndpoint, $id);
        return $this->client->get($uri, null, SymmetricDocument::class);
    }
}
