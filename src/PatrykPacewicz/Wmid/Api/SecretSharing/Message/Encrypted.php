<?php

namespace PatrykPacewicz\Wmid\Api\SecretSharing\Message;

use JMS\Serializer\Annotation\Type;

class Encrypted
{
    /** @Type("array<string>") */
    private $data;

    public function __construct(array $data)
    {
        $this->data = $data;
    }

    /** @return array */
    public function getData()
    {
        return $this->data;
    }
}
