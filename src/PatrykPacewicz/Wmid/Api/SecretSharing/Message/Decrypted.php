<?php

namespace PatrykPacewicz\Wmid\Api\SecretSharing\Message;

use JMS\Serializer\Annotation\Type;

class Decrypted
{
    /** @Type("string") */
    private $message;

    /** @Type("integer") */
    private $number;

    /**
     * @param string $message
     * @param int    $number
     */
    public function __construct($message, $number)
    {
        $this->message = $message;
        $this->number = $number;
    }

    /** @return string */
    public function getMessage()
    {
        return $this->message;
    }

    /** @return int */
    public function getNumber()
    {
        return $this->number;
    }
}
