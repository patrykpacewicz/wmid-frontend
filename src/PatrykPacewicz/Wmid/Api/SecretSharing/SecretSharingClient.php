<?php

namespace PatrykPacewicz\Wmid\Api\SecretSharing;

use PatrykPacewicz\Wmid\Api\ApiClient;
use PatrykPacewicz\Wmid\Api\SecretSharing\Message\Decrypted;
use PatrykPacewicz\Wmid\Api\SecretSharing\Message\Encrypted;

class SecretSharingClient
{
    /** @var ApiClient */
    private $client;

    /** @var string */
    private $encodeEndpoint;

    /** @var string */
    private $decodeEndpoint;

    public function __construct(ApiClient $client, $encodeEndpoint, $decodeEndpoint)
    {
        $this->client = $client;
        $this->encodeEndpoint = $encodeEndpoint;
        $this->decodeEndpoint = $decodeEndpoint;
    }

    public function encode (Decrypted $decrypted)
    {
        return $this->client->post($this->encodeEndpoint, $decrypted, Encrypted::class);
    }

    public function decode (Encrypted $encrypted)
    {
        return $this->client->post($this->decodeEndpoint, $encrypted, Decrypted::class);
    }
}
