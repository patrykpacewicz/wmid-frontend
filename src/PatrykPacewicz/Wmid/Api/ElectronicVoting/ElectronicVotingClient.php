<?php

namespace PatrykPacewicz\Wmid\Api\ElectronicVoting;

use PatrykPacewicz\Wmid\Api\ApiClient;
use PatrykPacewicz\Wmid\Api\ElectronicVoting\Message\VoteMessage;

class ElectronicVotingClient
{
    /** @var ApiClient */
    private $client;

    public function __construct(ApiClient $client)
    {
        $this->client = $client;
    }

    public function getVoteOptions()
    {
        return $this->client->get('voting/voting-center/options', null, 'array<string>');
    }

    public function getVoteResult()
    {
        return $this->client->get('voting/voting-center/vote', null, 'array<string, integer>');
    }

    public function vote(VoteMessage $voteMessage)
    {
        return $this->client->post('voting/voter/vote', $voteMessage, 'boolean');
    }
}
