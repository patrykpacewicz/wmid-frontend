<?php

namespace PatrykPacewicz\Wmid\Api\ElectronicVoting\Message;

use JMS\Serializer\Annotation\Type;

class VoteMessage
{
    /** @Type("integer") */
    private $vote;

    /** @Type("string") */
    private $signature;

    public function __construct($signature, $vote)
    {
        $this->signature = $signature;
        $this->vote      = (int) $vote;
    }

    /** @return mixed */
    public function getSignature()
    {
        return $this->signature;
    }

    /** @return mixed */
    public function getVote()
    {
        return $this->vote;
    }
}
