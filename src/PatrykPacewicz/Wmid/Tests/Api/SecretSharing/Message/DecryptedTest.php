<?php

namespace PatrykPacewicz\Wmid\Tests\Api\SecretSharing\Message;

use PatrykPacewicz\Wmid\Api\SecretSharing\Message\Decrypted;

class DecryptedTest extends \PHPUnit_Framework_TestCase
{
    /** @test */
    public function shouldGetMessage()
    {
        $anyMessage = "any message";
        $anyNumber = 3;
        $decrypted = new Decrypted($anyMessage, $anyNumber);

        $this->assertSame($anyMessage, $decrypted->getMessage());
    }

    /** @test */
    public function shouldGetNumber()
    {
        $anyMessage = "any message";
        $anyNumber = 3;
        $decrypted = new Decrypted($anyMessage, $anyNumber);

        $this->assertSame($anyNumber, $decrypted->getNumber());
    }
}
