<?php

namespace PatrykPacewicz\Wmid\Tests\Api\SecretSharing\Message;

use PatrykPacewicz\Wmid\Api\SecretSharing\Message\Encrypted;

class EncryptedTest extends \PHPUnit_Framework_TestCase
{
    /** @test */
    public function shouldGetData()
    {
        $anyData = array('any');
        $encrypted = new Encrypted($anyData);

        $this->assertSame($anyData, $encrypted->getData());
    }
}
