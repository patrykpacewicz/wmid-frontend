<?php

namespace PatrykPacewicz\Wmid\Tests\Api\SecretSharing;

use PatrykPacewicz\Wmid\Api\SecretSharing\Message\Decrypted;
use PatrykPacewicz\Wmid\Api\SecretSharing\Message\Encrypted;
use PatrykPacewicz\Wmid\Api\SecretSharing\SecretSharingClient;
use PatrykPacewicz\Wmid\Api\ApiClient;

class SecretSharingClientTest extends \PHPUnit_Framework_TestCase
{
    /** @test */
    public function shouldEncodeWithToApiClientDecryptedData()
    {
        $apiClientMock = $this->createApiClientMock();
        $encodeEndpoint = 'someEncodeEndpoint';
        $decodeEndpoint = 'someDecodeEndpoint';
        $decrypted = new Decrypted('anyMessage', 2);

        $client = new SecretSharingClient($apiClientMock, $encodeEndpoint, $decodeEndpoint);

        $apiClientMock->expects($this->once())
            ->method('post')
            ->with($encodeEndpoint, $decrypted, Encrypted::class);

        $client->encode($decrypted);
    }

    /** @test */
    public function shouldDecodeWithToApiClientEncryptedData()
    {
        $apiClientMock = $this->createApiClientMock();
        $encodeEndpoint = 'someEncodeEndpoint';
        $decodeEndpoint = 'someDecodeEndpoint';
        $encrypted = new Encrypted(array('any', 'data'));

        $client = new SecretSharingClient($apiClientMock, $encodeEndpoint, $decodeEndpoint);

        $apiClientMock->expects($this->once())
            ->method('post')
            ->with($decodeEndpoint, $encrypted, Decrypted::class);

        $client->decode($encrypted);
    }

    private function createApiClientMock()
    {
        return $this->getMockBuilder(ApiClient::class)
            ->disableOriginalConstructor()
            ->setMethods(array('post'))
            ->getMock();
    }
}
