<?php

namespace PatrykPacewicz\Wmid\Tests\Api\CommitmentScheme\Message;

use PatrykPacewicz\Wmid\Api\CommitmentScheme\Message\SymmetricDocument;

class SymmetricDocumentTest extends \PHPUnit_Framework_TestCase
{
    /** @test */
    public function shouldGetDocumentId()
    {
        $startMessage = new SymmetricDocument(123, null, null, null);
        $this->assertSame(123, $startMessage->getDocumentId());
    }

    /** @test */
    public function shouldGetEncodedMessage()
    {
        $startMessage = new SymmetricDocument(null, "encoded message", null, null);
        $this->assertSame("encoded message", $startMessage->getEncodedMessage());
    }

    /** @test */
    public function shouldGetKey()
    {
        $startMessage = new SymmetricDocument(null, null, 'any key', null);
        $this->assertSame('any key', $startMessage->getKey());
    }

    /** @test */
    public function shouldSetGetKey()
    {
        $startMessage = new SymmetricDocument(null, null, null, null);
        $startMessage->setKey('any key');
        $this->assertSame('any key', $startMessage->getKey());
    }

    /** @test */
    public function shouldGetMessage()
    {
        $startMessage = new SymmetricDocument(null, null, null, 'any message');
        $this->assertSame('any message', $startMessage->getMessage());
    }

    /** @test */
    public function shouldSetGetMessage()
    {
        $startMessage = new SymmetricDocument(null, null, null, null);
        $startMessage->setMessage('any message');
        $this->assertSame('any message', $startMessage->getMessage());
    }
}
