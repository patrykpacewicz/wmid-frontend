<?php

namespace PatrykPacewicz\Wmid\Tests\Api\CommitmentScheme\Message;

use PatrykPacewicz\Wmid\Api\CommitmentScheme\Message\StartMessage;

class StartMessageTest extends \PHPUnit_Framework_TestCase
{
    /** @test */
    public function shouldGetClientHost()
    {
        $startMessage = new StartMessage('any hostname', null, null);
        $this->assertSame('any hostname', $startMessage->getClientHost());
    }

    /** @test */
    public function shouldGetKey()
    {
        $startMessage = new StartMessage(null, 'any key', null);
        $this->assertSame('any key', $startMessage->getKey());
    }

    /** @test */
    public function shouldSetGetKey()
    {
        $startMessage = new StartMessage(null, null, null);
        $startMessage->setKey('any key');
        $this->assertSame('any key', $startMessage->getKey());
    }

    /** @test */
    public function shouldGetMessage()
    {
        $startMessage = new StartMessage(null, null, 'any message');
        $this->assertSame('any message', $startMessage->getMessage());
    }

    /** @test */
    public function shouldSetGetMessage()
    {
        $startMessage = new StartMessage(null, null, null);
        $startMessage->setMessage('any message');
        $this->assertSame('any message', $startMessage->getMessage());
    }
}
