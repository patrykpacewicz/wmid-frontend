<?php

namespace PatrykPacewicz\Wmid\Tests\Api\CommitmentScheme;

use PatrykPacewicz\Wmid\Api\ApiClient;
use PatrykPacewicz\Wmid\Api\CommitmentScheme\CommitmentSchemeClient;
use PatrykPacewicz\Wmid\Api\CommitmentScheme\Message\StartMessage;
use PatrykPacewicz\Wmid\Api\CommitmentScheme\Message\SymmetricDocument;

class CommitmentSchemeClientTest extends \PHPUnit_Framework_TestCase
{
    /** @test */
    public function shouldGetAllMessagesFromApi()
    {
        $apiClientMock = $this->createApiClientMock();

        $client = new CommitmentSchemeClient($apiClientMock, "endpoint", null, null, null);

        $apiClientMock->expects($this->once())
            ->method('get')
            ->with("endpoint", null, 'array<PatrykPacewicz\Wmid\Api\CommitmentScheme\Message\SymmetricDocument>');

        $client->getAllMessages();
    }

    /** @test */
    public function shouldValidateSendedApi()
    {
        $apiClientMock = $this->createApiClientMock();
        $symmetricDocument = new SymmetricDocument(null, null, null, null);

        $client = new CommitmentSchemeClient($apiClientMock, null, null, "endpoint", null);

        $apiClientMock->expects($this->once())
            ->method('post')
            ->with("endpoint", $symmetricDocument, 'boolean');

        $client->validate($symmetricDocument);
    }

    /** @test */
    public function shouldSendStartMessageToApi()
    {
        $apiClientMock = $this->createApiClientMock();
        $startMessage = new StartMessage(null, null, null);

        $client = new CommitmentSchemeClient($apiClientMock, null, "endpoint", null, null);

        $apiClientMock->expects($this->once())
            ->method('post')
            ->with("endpoint", $startMessage, 'PatrykPacewicz\Wmid\Api\CommitmentScheme\Message\SymmetricDocument');

        $client->send($startMessage);

    }

    /** @test */
    public function shouldGetItemData()
    {
        $apiClientMock = $this->createApiClientMock();

        $client = new CommitmentSchemeClient($apiClientMock, null, null, null, "endpoint/%s");

        $apiClientMock->expects($this->once())
            ->method('get')
            ->with("endpoint/itemId", null, 'PatrykPacewicz\Wmid\Api\CommitmentScheme\Message\SymmetricDocument');

        $client->getItem("itemId");

    }

    private function createApiClientMock()
    {
        return $this->getMockBuilder(ApiClient::class)
            ->disableOriginalConstructor()
            ->setMethods(array('post', 'get'))
            ->getMock();
    }
}
