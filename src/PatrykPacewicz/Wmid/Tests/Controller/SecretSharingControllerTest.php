<?php

namespace PatrykPacewicz\Wmid\Tests\Controller;

use PatrykPacewicz\Wmid\Api\SecretSharing\SecretSharingClient;
use PatrykPacewicz\Wmid\Controller\SecretSharingController;
use Symfony\Component\HttpFoundation\Request;

class SecretSharingControllerTest extends \PHPUnit_Framework_TestCase
{
    private $twig;
    private $client;
    private $secretSharingController;

    public function setUp()
    {
        $this->twig                    = $this->createTwigMock();
        $this->client                  = $this->createApiClientMock();
        $this->secretSharingController = new SecretSharingController($this->twig, $this->client);
    }

    /** @test */
    public function shouldReturnDefaultView()
    {
        $data = $this->secretSharingController->viewAction();
        $this->assertEmpty($data);
    }

    /** @test */
    public function shouldReturnEncodedView()
    {
        $request = new Request(array(), array('data' => 'data', 'number' => 'number'));
        $data = $this->secretSharingController->encodeAction($request);

        $this->assertArrayHasKey('encrypted', $data);
    }

    /** @test */
    public function shouldReturnDecodedView()
    {
        $request = new Request(array(), array('secret' => array()));
        $data = $this->secretSharingController->decodeAction($request);

        $this->assertArrayHasKey('decrypted', $data);
    }

    private function createTwigMock()
    {
        $twigMock = $this->getMockBuilder('Twig_Environment')
            ->disableOriginalConstructor()
            ->setMethods(array('render'))
            ->getMock();

        $twigMock->expects($this->once())
            ->method('render')
            ->will($this->returnArgument(1));

        return $twigMock;
    }

    private function createApiClientMock()
    {
        return $this->getMockBuilder(SecretSharingClient::class)
            ->disableOriginalConstructor()
            ->setMethods(array('encode', 'decode'))
            ->getMock();
    }
}
