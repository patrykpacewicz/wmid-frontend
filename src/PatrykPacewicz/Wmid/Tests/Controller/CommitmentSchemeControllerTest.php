<?php

namespace PatrykPacewicz\Wmid\Tests\Controller;

use PatrykPacewicz\Wmid\Api\CommitmentScheme\CommitmentSchemeClient;
use PatrykPacewicz\Wmid\Controller\CommitmentSchemeController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Generator\UrlGenerator;

class CommitmentSchemeControllerTest extends \PHPUnit_Framework_TestCase
{
    private $twig;
    private $client;
    private $urlGenerator;
    private $commitmentSchemeController;

    public function setUp()
    {
        $this->twig                    = $this->createTwigMock();
        $this->client                  = $this->createApiClientMock();
        $this->urlGenerator            = $this->createUrlGeneratorMock();
        $this->commitmentSchemeController = new CommitmentSchemeController(
            $this->twig,
            $this->client,
            $this->urlGenerator
        );
    }

    /** @test */
    public function shouldReturnDefaultView()
    {
        $data = $this->commitmentSchemeController->viewAction();
        $this->assertArrayHasKey('messages', $data);
    }

    /** @test */
    public function shouldSendStartMessage()
    {
        $request = new Request([], ['data' => 'data', 'secret' => 'secret', 'host' => 'host']);
        $data = $this->commitmentSchemeController->sendAction($request);

        $this->assertArrayHasKey('messages', $data);
        $this->assertArrayHasKey('result', $data);
    }

    /** @test */
    public function shouldGetValidate()
    {
        $data = $this->commitmentSchemeController->getValidateAction(123123);

        $this->assertArrayHasKey('document', $data);
        $this->assertArrayHasKey('isValid', $data);
        $this->assertSame(true, $data['isValid']);
    }

    /** @test */
    public function shouldRedirectIfValidate()
    {
        $request = new Request([], ['data' => 'data', 'secret' => 'secret', 'encoded' => 'encoded']);

        $this->client->expects($this->once())->method('validate')->will($this->returnValue(true));
        $this->urlGenerator->expects($this->once())->method('generate')->will($this->returnArgument(0));

        $data = $this->commitmentSchemeController->validateAction(123123, $request);

        $this->assertSame('commitmentscheme', $data->getTargetUrl());
    }

    /** @test */
    public function shouldGetValidateIfNotValidate()
    {
        $request = new Request([], ['data' => 'data', 'secret' => 'secret', 'encoded' => 'encoded']);

        $this->client->expects($this->once())->method('validate')->will($this->returnValue(false));

        $data = $this->commitmentSchemeController->validateAction(123123, $request);

        $this->assertArrayHasKey('document', $data);
        $this->assertArrayHasKey('isValid', $data);
        $this->assertSame(false, $data['isValid']);
    }


    private function createTwigMock()
    {
        $twigMock = $this->getMockBuilder('Twig_Environment')
            ->disableOriginalConstructor()
            ->setMethods(array('render'))
            ->getMock();

        $twigMock->expects($this->any())
            ->method('render')
            ->will($this->returnArgument(1));

        return $twigMock;
    }

    private function createApiClientMock()
    {
        return $this->getMockBuilder(CommitmentSchemeClient::class)
            ->disableOriginalConstructor()
            ->setMethods(array('getAllMessages', 'send', 'getItem', 'validate'))
            ->getMock();
    }

    private function createUrlGeneratorMock()
    {
        return $this->getMockBuilder(UrlGenerator::class)
            ->disableOriginalConstructor()
            ->setMethods(array())
            ->getMock();
    }
}
