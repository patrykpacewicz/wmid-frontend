<?php

namespace PatrykPacewicz\Wmid\Tests\Controller;

use PatrykPacewicz\Wmid\Controller\DefaultController;

class DefaultControllerTest extends \PHPUnit_Framework_TestCase
{
    /** @test */
    public function shouldReturnHelloWorldInMessage()
    {
        $twigMock = $this->createTwigMock();
        $defaultController = new DefaultController($twigMock);
        $data = $defaultController->defaultAction();

        $this->assertCount(0, $data);
    }

    private function createTwigMock()
    {
        $twigMock = $this->getMockBuilder('Twig_Environment')
            ->disableOriginalClone()
            ->setMethods(array('render'))
            ->getMock();

        $twigMock->expects($this->once())
            ->method('render')
            ->will($this->returnArgument(1));

        return $twigMock;
    }
}
