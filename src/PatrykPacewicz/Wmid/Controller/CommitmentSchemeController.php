<?php

namespace PatrykPacewicz\Wmid\Controller;

use PatrykPacewicz\Wmid\Api\CommitmentScheme\CommitmentSchemeClient;
use PatrykPacewicz\Wmid\Api\CommitmentScheme\Message\StartMessage;
use PatrykPacewicz\Wmid\Api\CommitmentScheme\Message\SymmetricDocument;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Generator\UrlGenerator;
use Twig_Environment;

class CommitmentSchemeController
{
    /** @var Twig_Environment */
    private $twig;

    /** @var CommitmentSchemeClient */
    private $client;

    /** @var UrlGenerator */
    private $urlGenerator;

    /** @var bool */
    private $isValid = true;

    public function __construct(
        Twig_Environment $twig,
        CommitmentSchemeClient $client,
        UrlGenerator $urlGenerator
    ) {
        $this->twig = $twig;
        $this->client = $client;
        $this->urlGenerator = $urlGenerator;
    }

    public function viewAction()
    {
        $parameters = ['messages' => $this->client->getAllMessages()];
        return $this->twig->render('CommitmentScheme\index.html.twig', $parameters);
    }

    public function sendAction(Request $request)
    {
        $data   = $request->request->get('data');
        $secret = $request->request->get('secret');
        $host   = $request->request->get('host');

        $parameters = [
            'result' => $this->client->send(new StartMessage($host, $secret, $data)),
            'messages' => $this->client->getAllMessages(),
        ];

        return $this->twig->render('CommitmentScheme\send.html.twig', $parameters);
    }

    public function getValidateAction($documentId)
    {
        $parameters = [
            'document' => $this->client->getItem($documentId),
            'isValid' => $this->isValid,
        ];
        return $this->twig->render('CommitmentScheme\validate.html.twig', $parameters);
    }

    public function validateAction($documentId, Request $request)
    {
        $data    = $request->request->get('data');
        $secret  = $request->request->get('secret');
        $encoded = $request->request->get('encoded');

        $symmetricDocument = new SymmetricDocument($documentId, $encoded, $secret, $data);
        $isValid = $this->client->validate($symmetricDocument);

        if (!$isValid) {
            $this->isValid = false;
            return $this->getValidateAction($documentId);
        }

        return new RedirectResponse($this->urlGenerator->generate('commitmentscheme'));
    }
}
