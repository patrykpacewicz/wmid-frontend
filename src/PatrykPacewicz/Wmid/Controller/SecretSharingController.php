<?php

namespace PatrykPacewicz\Wmid\Controller;

use PatrykPacewicz\Wmid\Api\SecretSharing\Message\Encrypted;
use PatrykPacewicz\Wmid\Api\SecretSharing\Message\Decrypted;
use PatrykPacewicz\Wmid\Api\SecretSharing\SecretSharingClient;
use Symfony\Component\HttpFoundation\Request;
use \Twig_Environment;

class SecretSharingController
{
    /** @var Twig_Environment */
    private $twig;

    /** @var SecretSharingClient */
    private $client;

    public function __construct(Twig_Environment $twig, SecretSharingClient $client)
    {
        $this->twig = $twig;
        $this->client = $client;
    }

    public function viewAction()
    {
        return $this->twig->render('SecretSharing\index.html.twig');
    }

    public function encodeAction(Request $request)
    {
        $data   = $request->request->get('data');
        $number = $request->request->get('number');

        $parameters = ['encrypted' => $this->client->encode(new Decrypted($data, $number))];
        return $this->twig->render('SecretSharing\encrypted.html.twig', $parameters);
    }

    public function decodeAction(Request $request)
    {
        $secret = $request->request->get('secret');
        $secret = array_filter($secret);

        $parameters = ['decrypted' => $this->client->decode(new Encrypted($secret))];
        return $this->twig->render('SecretSharing\decrypted.html.twig', $parameters);
    }
}
