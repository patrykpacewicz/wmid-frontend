<?php

namespace PatrykPacewicz\Wmid\Controller;

use \Twig_Environment;

class DefaultController
{
    /** @var Twig_Environment */
    private $twig;

    public function __construct(Twig_Environment $twig)
    {
        $this->twig = $twig;
    }

    public function defaultAction()
    {
        return $this->twig->render('index.html.twig');
    }

    public function imageProcessingAction()
    {
        return $this->twig->render('ImageProcessing/index.html.twig');
    }
}
