<?php

namespace PatrykPacewicz\Wmid\Controller;

use Guzzle\Http\Exception\ClientErrorResponseException;
use PatrykPacewicz\Wmid\Api\ElectronicVoting\ElectronicVotingClient;
use PatrykPacewicz\Wmid\Api\ElectronicVoting\Message\VoteMessage;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Generator\UrlGenerator;
use Twig_Environment;

class ElectronicVotingController
{
    /** @var Twig_Environment */
    private $twig;

    /** @var ElectronicVotingClient */
    private $client;

    /** @var UrlGenerator */
    private $urlGenerator;

    public function __construct(
        Twig_Environment $twig,
        ElectronicVotingClient $client,
        UrlGenerator $urlGenerator
    ) {
        $this->twig = $twig;
        $this->client = $client;
        $this->urlGenerator = $urlGenerator;
    }

    public function viewAction()
    {
        $parameters = [
            'voteOptions' => $this->client->getVoteOptions(),
            'pieData' => $this->mapToPieData($this->client->getVoteResult()),
        ];

        return $this->twig->render('ElectronicVoting\index.html.twig', $parameters);
    }


    public function voteAction(Request $request)
    {
        $sign    = $request->request->get('sign');
        $option  = $request->request->get('option');

        try {
            $this->client->vote(new VoteMessage($sign, $option));
        } catch (ClientErrorResponseException $ex) {
            if ($ex->getResponse()->getStatusCode() !== 403) {
                throw $ex;
            }

            return $this->twig->render('error.html.twig', ['code' => 403, 'message' => 'Forbidden: you can not vote']);
        }

        return new RedirectResponse($this->urlGenerator->generate('voting'));
    }

    private function mapToPieData(array $data)
    {
        $pieData = [];

        foreach ($data as $name => $value) {
            $pieData[] = ['name' => $name, 'value' => $value];
        }

        return $pieData;
    }
}
