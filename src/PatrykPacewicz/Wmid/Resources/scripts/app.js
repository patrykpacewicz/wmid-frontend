require(
    [
        'jquery',
        'imageprocessing/app',
        'chart/chart',
        'bootstrap',
    ],
    function ($, imageFillApp, Chart) {
        if ($('#electronicvoting-pie').length ) {
            var chart = new Chart('#electronicvoting-pie');
            chart.pie(300);
        }

        if ($('#matrix').length ) {
            imageFillApp('#matrix');
        }
    }
);
