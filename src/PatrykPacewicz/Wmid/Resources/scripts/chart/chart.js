define(
    [
        'crossfilter',
        'dc',
        'jquery'
    ],
    function (crossfilter, dc, $) {
        function Chart(element) {
            this.element = element;
        }

        Chart.prototype.pie = function() {
            var data          = $(this.element).data('chart');
            var chart         = dc.pieChart(this.element);
            var runDimension  = crossfilter(data).dimension(function(d) {return d.name;});
            var speedSumGroup = runDimension.group().reduceSum(function(d) {return d.value;});

            chart
                .innerRadius(50)
                .dimension(runDimension)
                .group(speedSumGroup)
                .render();
        }

        return Chart;
    }
);
