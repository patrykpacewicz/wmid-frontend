define(
    [
        'imageprocessing/view/ImageFill',
        'imageprocessing/model/Matrix',
        'imageprocessing/action/Draw',
        'imageprocessing/action/Fill',
    ],
    function (ImageFillView, MatrixModel, DrawAction, FillAction) {
        return function(matrixElement) {
            var matrixLittle = new MatrixModel(matrixElement + " .matrix-trans")
            var matrix = new MatrixModel(matrixElement + " .matrix");

            var imageFillView = new ImageFillView({matrix: matrix, el: matrixElement + " .matrix"});
            imageFillView.addAction(new DrawAction());
            imageFillView.addAction(new FillAction(matrixLittle));

            var matrixLittleView = new ImageFillView({matrix: matrixLittle, el: matrixElement + " .matrix-trans"})
            matrixLittleView.addAction(new DrawAction());

        }
    }
);
