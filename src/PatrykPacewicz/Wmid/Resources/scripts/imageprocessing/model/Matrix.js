define(
    [
        'jquery',
    ],
    function ($) {
        function Matrix(element) {
            this.$el = $(element);
        }

        Matrix.prototype.isActive = function(x, y) {
            return this.getElement(x, y).hasClass('active');
        }

        Matrix.prototype.toogleActive = function(x, y) {
            return this.getElement(x, y).toggleClass('active');
        }

        Matrix.prototype.getElement = function(x, y) {
            return this.$el.find('[data-x='+ x +'][data-y='+ y +']');
        }

        Matrix.prototype.toArray = function() {
            var result = [],
                rowResult, i, j;

            for (i = 0 ; i < this.getWidth(); i++) {
                rowResult = [];
                for (j = 0; j < this.getHeight(); j++) {
                    rowResult.push(this.isActive(i, j));
                }
                result.push(rowResult);
            }

            return result;
        }

        Matrix.prototype.getWidth = function() {
            return this.getRow(0).length;
        }

        Matrix.prototype.getHeight = function() {
            return this.getRows().length;
        }

        Matrix.prototype.getRows = function getRows() {
            return this.$el.find('.matrix-row');
        }

        Matrix.prototype.getRow = function (rowNumber) {
            return $(this.$el).find('[data-y='+ rowNumber +']')
        };

        return Matrix;
    }
);
