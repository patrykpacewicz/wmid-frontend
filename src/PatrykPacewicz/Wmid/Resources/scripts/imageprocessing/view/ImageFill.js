define(
    [
        'backbone',
        'jquery',
        'underscore'
    ],
    function (Backbone, $, _) {
        return Backbone.View.extend({
            events: {
                "click .matrix-cell":                       "cellAction",
                "click .processing-buttons a[type=button]": "changeActiveState"
            },

            initialize: function(options) {
                this.$buttons = this.$el.find('a[type=button]');
                this.matrix = options.matrix;
                this.buildType = 'draw';
                this.actions = [];
            },

            changeActiveState: function(event) {
                this.$buttons.toggleClass('active');
                this.buildType = this.$buttons.filter('.active').data('type');
            },

            addAction: function(action) {
                this.actions.push(action);
            },

            cellAction: function(event) {
                x = $(event.currentTarget).data("x");
                y = $(event.currentTarget).data("y");

                _.each(this.actions, function(element) {
                    if (element.getName() === this.buildType) {
                        element.action(this.matrix, x, y);
                    }
                }, this);
            }
        });
    }
);
