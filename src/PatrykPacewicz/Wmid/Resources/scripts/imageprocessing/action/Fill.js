define(
    [
        'underscore',
    ],
    function (_) {
        function Fill(matrixLittle) {
            this.matrixLittle = matrixLittle;
        }

        Fill.prototype.action = function(matrix, x, y) {
            this.matrix = matrix;
            this.state = this.matrix.isActive(x, y);

            this.fillElements(x, y);
        }

        Fill.prototype.getName = function() {
            return 'fill';
        }

        Fill.prototype.fillElements = function(x, y) {
            if (!this.validate(x, y)) {
                return;
            }

            var positions = this.getPositions();

            for (var i=0 ; i<positions.length ; i++) {
                if (this.matrix.isActive(x + positions[i].x, y + positions[i].y) === this.state) {
                    this.matrix.toogleActive(x + positions[i].x, y + positions[i].y);
                    positions[i].is = true;
                }
            }

            for (var i=0 ; i<positions.length ; i++) {
                if (positions[i].is) {
                    this.fillElements(x + positions[i].x, y + positions[i].y);
                }
            }
        }

        Fill.prototype.getPositions = function() {
            var resultMatrix = [];
            var matrixOfChenges = this.matrixLittle.toArray();

            for (var i = 0; i < 3; i++) {
                for (var j = 0; j < 3; j++) {
                    if (matrixOfChenges[i][j]) {
                        resultMatrix.push({x: j-1, y: i-1, is: false});
                    }
                }
            }

            return resultMatrix;
        }

        Fill.prototype.validate = function(x, y) {
            if (x < 0 || x >= this.matrix.getWidth()) {
                return false;
            }

            if (y < 0 || y >= this.matrix.getHeight()) {
                return false;
            }

            return true;
        }

        return Fill;
    }
);
