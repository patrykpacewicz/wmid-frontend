define(
    [
        'jquery',
    ],
    function ($) {
        function Draw() {}

        Draw.prototype.action = function(matrix, x, y) {
            matrix.toogleActive(x, y);
        }

        Draw.prototype.getName = function() {
            return 'draw';
        }

        return Draw;
    }
);
